# DOCKER CHEATSHEET

## Table of Contents

**[Install Docker / Docker cli](#Install-Docker-/-Docker-cli)**<br>
**[Basics : Deploy a container](#Basics-:-Deploy-a-container)**<br>
**[Troubleshooting](#troubleshooting)**<br>
**[Compatibility](#compatibility)**<br>
**[Notes and Miscellaneous](#notes-and-miscellaneous)**<br>
**[Building the Extension Bundles](#building-the-extension-bundles)**<br>
**[Next Steps, Credits, Feedback, License](#next-steps)**<br>
**[Basics : Deploy a container](#Basics-:-Deploy-a-container)**<br>
**[Build a docker image running Nginx with HTML site](#Build-a-docker-image-running-Nginx-with-HTML-site)**<br>
**[Building Container Images](#Building-Container-Images)**<br>
**[Dockerizing Node.js applications](#Dockerizing-Node-js-applications)**<br>
**[Optimising Dockerfile with OnBuild](#Optimising-Dockerfile-with-OnBuild)**<br>
**[Ignoring Files](#Ignoring-Files)**<br>
**[Data containers](#Data-containers)**<br>
**[Communicating Between Containers](#Communicating-Between-Containers)**<br>
**[Docker Networks](#Docker-Networks-)**<br>
**[Persisting Data Using Volumes](#Persisting-Data-Using-Volumes)**<br>
**[Managing Log Files](#Managing-Log-Files)**<br>
**[Ensuring Uptime (_assurer la disponibilité_)](<#Ensuring-Uptime-(_assurer-la-disponibilité_)>)**<br>
**[Docker Metadata & Labels](#Docker-Metadata-&-Labels)**<br>
**[Load Balancing Containers](#Load-Balancing-Containers)**<br>

## Install Docker / Docker cli

https://docs.docker.com/docker-for-windows/install/

## Basics : Deploy a container

- Find a docker image `docker search <name>`

  ```sh
  docker search redis
  ```

- Running an instance of a container's image `docker run <option> <image-name>`

  ```sh
  docker run -d redis
  ```

  Without `-d` flag docker will launch in the foreground. To run in the background, the option -d needs to be specified.
  By default, Docker will run the latest version available. If a particular version was required, it could be specified as a tag, for example, version 3.2 would be `docker run -d redis:3.2`.

- Lists all running containers

  ```sh
  docker ps
  ```

  This command also displays the friendly name and ID that can be used to find out information about individual containers.

- Lists all containers ( running or not)

  ```sh
  docker ps -a
  ```

* More detail on a container `docker inspect <name|container-id>`
  ```sh
  docker inspect redis
  ```
* Display logs from a container `docker logs <name|container-id>`

  ```sh
  docker inspect e15c8b174b56
  ```

* Give a name to a instance `docker run --name <name>`

* Bound port from host to container :
  ports are bound when containers are started using `-p <host-port>:<container-port>`
  option

> _Protip_
>
> - By default, the port on the host is mapped to 0.0.0.0, which means all IP addresses. You can specify a particular IP address when you define the port mapping, for example, -p 127.0.0.1:6379:6379
> - The problem with running processes on a fixed port is that you can only run one instance. the option -p 6379 without <container-port> enables to expose container but on a randomly available port. To dicover witch one `docker port <container-name> <host-port>`

- Persisting data :
  Containers are designed to be stateless. Binding directories (also known as volumes) is done using the option `-v <host-dir>:<container-dir>`

> _Protip_
> Docker allows you to use \$PWD as a placeholder for the current directory.

- Access to a container bash shell :
  to interact with the container (for example, to access a bash shell), include the options `-it`.
  `docker run -it ubuntu bash` give access t a bash shell inside of a container.
  `docker run ubuntu ps` launches an ubuntu container and execute de command ps.

## Build a docker image running Nginx with HTML site

Docker Images start from a base image. The base image should include the platform dependencies required by your application, for example, having the JVM or CLR installed.
This base image is defined as an instruction in the Dockerfile. Docker Images are built based on the contents of a Dockerfile. The Dockerfile is a list of instructions describing how to deploy your application.
In this example, our base image is the Alpine version of Nginx. This provides the configured web server on the Linux Alpine distribution.

- Create a Dockerfile

```sh
FROM nginx:alpine  //base image
COPY . /usr/share/nginx/html //copies the content of the current directory into a particular location inside the container
```

- Build Docker Image
  `docker build -t <build-directory>`. The -t parameter allows you to specify a friendly name for the image and a tag, commonly used as a version number

  ```sh
  docker build -t webserver-image:v1 .
  ```

- Display list of image

  ```
  docker images
  ```

- Starting nginx container

  ```
  docker run -d -p 80:80 webserver-image:v1
  ```

- Access the results of docker port
  ```sh
  curl docker
  ```

## Building Container Images

Docker images are built based on a Dockerfile. A Dockerfile defines all the steps required to create a Docker image with your application configured and ready to be run as a container. The image itself contains everything, from operating system to dependencies and configuration required to run your application.
**The Dockerfile allows for images to be composable, enabling users to extend existing images instead of building from scratch.**

1. **Base images**

   All Docker images start from a base image. A base image is the same images from the Docker Registry which are used to start containers. Along with the image name, we can also include the image tag to indicate which particular version we want, by default, this is latest.

To define a base image we use the instruction in **Dockerfile**
`FROM <image-name>:<tag>`
Dockerfile's are simple text files with a command on each line.

```sh
FROM nginx:1.11-alpin
```

2.  **Running Commands**

    The main commands two are COPY and RUN.

- **RUN** <command> allows you to execute any command as you would at a command prompt, for example installing different application packages or running a build command. The results of the RUN are persisted to the image so it's important not to leave any unnecessary or temporary files on the disk as these will be included in the image.

- **COPY** <src> <dest> allows you to copy files from the directory containing the Dockerfile to the container's image. This is extremely useful for source code and assets that you want to be deployed inside your container.
  ```sh
  COPY ./index.html /usr/share/nginx/html
  ```

> _Protip_
> If you're copying a file into a directory then you need to specify the filename as part of the destination.

3. **Exposing Ports**

   Using the EXPOSE <port> command you tell Docker which ports should be open and can be bound too. You can define multiple ports on the single command, for example, `EXPOSE 80 433 or EXPOSE 7000-8000`

```sh
ESPOSE 80
```

4. **Default Commands**

   The CMD line in a Dockerfile defines the default command to run when a container is launched. If the command requires arguments then it's recommended to use an array, for example ["cmd", "-a", "arga value", "-b", "argb-value"], which will be combined together and the command cmd -a "arga value" -b argb-value would be run.

   ```sh
   CMD ["nginx", "-g", "daemon off;"]
   ```

> _Protip_
> An alternative approach to CMD is ENTRYPOINT. While a CMD can be overridden when the container starts, a ENTRYPOINT defines a command which can have arguments passed to it when the container launches.
> In this example, NGINX would be the entrypoint with -g daemon off; the default command.

5. **Building Containers**

   After writing your Dockerfile you need to use `docker build <build-directory>` to turn it into an image. The `build` command takes in a directory containing the Dockerfile, executes the steps and stores the image in your local Docker Engine. If one fails because of an error then the build stops.

   ```sh
   docker build -t nginx-server:lastest .
   ```

6. **Launching New Image**

   NGINX is designed to run as a background service so you should include the option -d. To make the web server accessible, bind it to port 80 using p 80:80

```sh
docker run -d -p 80:80 <image-id|friendly-tag-name>
```

    You can access the launched web server via the hostname docker. After launching the container, the command `curl -i http://docker` will return our index file via NGINX and the image we built.

## Dockerizing Node.js applications

Starting with a default Expressjs application in the working directory

1. **Base image**

   Create a Dockerfile, we also need to create the base directories of where the application runs from. Using the RUN <command> we can execute commands as if they're running from a command shell, by using mkdir we can create the directories where the application will execute from. In this case, an ideal directory would be /src/app as the environment user has read/write access to this directory.

   We can define a working directory using WORKDIR <directory> to ensure that all future commands are executed from the directory relative to our application.

```sh
FROM node:10-alpine
RUN mkdir -p src/app
WORKDIR src/app
```

2. **NPM Install**

   Install the dependencies required to run the application. For Node.js this means running NPM install.

   To keep build times to a minimum, Docker caches the results of executing a line in the Dockerfile for use in a future build. If something has changed, then Docker will invalidate the current and all following lines to ensure everything is up-to-date.

   With NPM we only want to re-run `npm install` if something within our package.json file has changed. If nothing has changed then we can use the cache version to speed up deployment. By using COPY package.json <dest> we can cause the RUN npm install command to be invalidated if the package.json file has changed.

```sh
COPY package.json /src/app/package.json
RUN npm install
```

> _Protip_
> If you don't want to use the cache as part of the build then set the option --no-cache=true as part of the docker build command.

3. **Configuring Application**

   After we've installed our dependencies, we want to copy over the rest of our application's source code. Splitting the installation of the dependencies and copying out source code enables us to use the cache when required.
   If we copied our code before running _npm install_ then it would run every time as our code would have changed. By copying just `package.json` we can be sure that the cache is invalidated only when our package contents have changed.

   ```sh
   [...]
   RUN npm install
   COPY . /src/app
   EXPOSE 3000
   CMD ["npm", "start"]
   ```

4. **Building & Launching Container**

- build the image : `docker build -t nodejs-app .`
- launch the built image : `docker run -d --name my-app -p 3000:3000 nodejs-app`
- testing container : curl http://docker:3000

5. **Environment Variables**

   Docker images should be designed that they can be transferred from one environment to the other without making any changes or requiring to be rebuilt. By following this pattern you can be confident that if it works in one environment, such as staging, then it will work in another, such as production.
   With Docker, environment variables can be defined when you launch the container. For example with Node.js applications, you should define an environment variable for NODE*ENV when running in production.
   Using *-e\_ option, you can set the name and value as `-e NODE_ENV=production`

   exemple `docker run -d --name my-production-running-app -e NODE_ENV=production -p 3000:3000 my-nodejs-app`

## Optimising Dockerfile with OnBuild

How you can optimise Dockerfile using the OnBuild instruction.

1. **OnBuild**

   While Dockerfile's are executed in order from top to bottom, you can trigger an instruction to be executed at a later time when the image is used as the base for another image.
   The result is you can delay your execution to be dependent on the application which you're building, for example the application's package.json file.

   ```sh
   FROM node:7
   RUN mkdir -p /usr/src/app
   WORKDIR /usr/src/app
   ONBUILD COPY package.json /usr/src/app/
   ONBUILD RUN npm install
   ONBUILD COPY . /usr/src/app
   CMD [ "npm", "start" ]
   ```

   The result is that we can build this image but the application specific commands won't be executed until the built image is used as a base image. They'll then be executed as part of the base image's build.

2. **Application Dockerfile**

   With all of the logic to copy the code, install our dependencies and launch our application the only aspect which needs to be defined on the application level is which port(s) to expose.

   The advantage of creating OnBuild images is that our Dockerfile is now much simpler and can be easily re-used across multiple projects without having to re-run the same steps improving build times.

   ```sh
   FROM node:7-onbuild // onbuild refere to the image
   EXPOSE 3000
   ```

3. **Building & Launching Container**

   You can build & launch your container normaly with `docker build -t my-nodejs-app .` & `docker run -d --name my-running-app -p 3000:3000 my-nodejs-app`

## Ignoring Files

You can ignore certain files from ending up inside a Docker image that can introduce security risks. we also investigates how you can reduce the build time by ignoring which files are sent to the Docker Build Context.

1. **Docker Ignore**

   To prevent sensitive files or directories from being included by mistake in images, you can add a file named .dockerignore.

   ```sh
   echo passwords.txt >> .dockerignore
   ```

   > _Protip_
   > If you need to use the passwords as part of a RUN command then you need to copy, execute and delete the files as part of a single RUN command. Only the final state of the Docker container is persisted inside the image.

2. **Docker Build Context**

   The .dockerignore file can ensure that sensitive details are not included in a Docker Image. However they can also be used to improve the build time of images.

   Add to .dockerignore :

   - .git
   - node_modules

   > _Protip_
   > It's wise to ignore .git directories along with dependencies that are downloaded/built within the image such as node_modules. These are never used by the application running within the Docker Container and just add overhead to the build process.

3. **Optimised Build**

   we used the .dockerignore file to exclude sensitive files, we can use it to exclude files which we don't want to be sent to the Docker Build Context during the build.
   To speed up our build, simply include the filename of the large file in the ignore file.

   ```sh
   echo big-temp-file.img >> .dockerignore
   ```

## Data containers

There are two ways of approaching stateful Containers, that is containers are store and persistent data for future use. This could be the container creating and storing data, for example, a database. Alternatively, it could be data requiring additional for instance the configuration or SSL certifications. This approach can also be used to backup data or debug containers.
`-v <host-dir>:<container-dir>` option to map directories. The other approach is to use Data Containers.

1. **Create Contianer**

   Data Containers are containers whose sole responsibility is to be a place to store/manage data.
   Like other containers they are managed by the host system. However, they don't run when you perform a `docker ps` command.
   we will use **busybox** as the base as it's small and lightweight in case we want to explore and move the container to another host.

   ```sh
   docker create -v /config --name dataContainer busybox
   ```

   `-v` option to define where other containers will be reading/saving data.

2. **Copy File**

   To copy files into a container you use the command docker cp. The following command will copy the config.conf file into our dataContainer and the directory config.

   ```sh
   docker cp config.conf dataContainer:/config/
   ```

3. **Mount Volumes From**

   Now our Data Container has our config, we can reference the container when we launch dependent containers requiring the configuration file.
   Using the `--volumes-from <container> option` we can use the mount volumes from other containers inside the container being launched.

   ```sh
   docker run --volumes-from dataContainer ubuntu ls /config
   ```

4. **Export / Import Containers**

   If we wanted to move the Data Container to another machine then we can export it to a .tar file.

   ```sh
   docker export dataContainer > dataContainer.tar
   ```

   The command `docker import dataContainer.tar` will import the Data Container back into Docker.

## Communicating Between Containers

How to allow multiple containers to communicate with each other. The steps will explain how to connect a data-store, in this case, Redis, to an application running in a separate container.

1. **Start Redis**

   ```sh
   docker run -d --name redis-server redis
   ```

2. **Create Link**

   To connect to a source container you use the `--link <container-name|id>:<alias>` option when launching a new container.

   - Docker will set some environment variables
     ```sh
     docker run --link redis-server:redis alpine env
     ```
   - Docker will update the HOSTS file of the container
     ```sh
     docker run --link redis-server:redis alpine cat /etc/hosts
     ```

   Test your link with a ping : `docker run --link redis-server:redis alpine ping -c 1 redis`

3. **Connect To App**

   Here is a simple node.js application which connects to redis using the hostname redis.

   ```sh
   docker run -d -p 3000:3000 --link redis-server:redis katacoda/redis-node-docker-example
   ```

   Test connection : `curl docker:3000`

4. **Connect to Redis CLI**

   In the same way, you can connect source containers to applications, you can also connect them to their own CLI tools.

   ```sh
   docker run -it --link redis-server:redis redis redis-cli -h redis
   ```

   > The command `KEYS *` will output the contents stored currently in the source redis container.
   > Type QUIT to exit the CLI.

## Docker Networks

How to create a docker network allowing containers to communicate. We'll also explore the Embedded DNS Server added in Docker 1.10.
Docker has two approaches to networking. The first defines a link between two containers. This link updates _/etc/hosts_ and environment variables to allow containers to discover and communicate.
The alternate approach is to create a _docker network_ that containers are connected to. The network has similar attributes to a physical network, allowing containers to come and go more freely than when using links

1. **Create Network**

   The first step is to create a network using the CLI. This network will allow us to attach multiple containers which will be able to discover each other

   ```sh
   docker network create backend-network
   ```

2. **Connect to Network**

   Containers can communicate via an Embedded DNS Server in Docker. This DNS server is assigned to all containers via the IP 127.0.0.11 and set in the resolv.conf file.
   When containers attempt to access other containers via a well-known name, such as Redis, the DNS server will return the IP address of the correct Container. In this case, the fully qualified name of Redis will be redis.backend-network.

   We can use the `--net` attribute to assign which network they should be connected to.

   ```sh
   docker run -d --name=redis --net=backend-network redis
   // show DNS server
   docker run --net=backend-network alpine cat /etc/resolv.conf
   // ping redis servir from alpine container
   docker run --net=backend-network alpine ping -c1 redis
   ```

3. **Connect Two Containers**

   Docker supports multiple networks and containers being attached to more than one network at a time.

   ```sh
   docker network create frontend-network

   docker network connect frontend-network redis

   docker run -d -p 3000:3000 --net=frontend-network katacoda/redis-node-docker-example
   ```

4. **Create Aliases**

   Links are still supported when using _docker network_ and provide a way to define an Alias to the container name. This will give the container an extra DNS entry name and way to be discovered. When using `--link` the embedded DNS will guarantee that localised lookup result only on that container where the `--link` is used.

   The other approach is to provide an alias when connecting a container to a network.

   - **Connect Container with Alias** : connect our Redis instance to the frontend-network with the alias of db.

   ```sh
   docker network create frontend-network2

   docker network connect --alias db frontend-network2 redis

   //testing
   docker run --net=frontend-network2 alpine ping -c1 db
   ```

5. **Explore and Disconnect Containers**

   - List all the network

   ```sh
   docker network ls
   ```

   - Explore a specific network

   ```sh
   docker network inspect frontend-network
   ```

   - Disconnect to a network

   ```sh
   docker network disconnect frontend-network redis
   ```

## Persisting Data Using Volumes

Docker Volumes allow directories to be shared between containers and container versions.
Docker Volumes allows you to upgrade containers, restart machines and share data without data loss. This is essential when updating database or application versions.

1. **Data Volumes**
   Docker Volumes are created and assigned when containers are started. Data Volumes allow you to map a host directory to a container for sharing data.
   This mapping is bi-directional. It allows data stored on the host to be accessed from within the container. It also means data saved by the process inside the container is persisted on the host.

   This example will use Redis as a way to persist data. Start a Redis container below, and create a data volume using the -v parameter. This specifies that any data saved inside the container to the _/data_ directory should be persisted on the host in the directory _/docker/redis-data_.

   ```sh
   docker run -v /docker/redis-data:/data \
    --name r1 -d redis \
    redis-server --appendonly yes
   ```

   We can pipe data into the Redis instance using the following command.

   ```sh
   cat data | docker exec -i r1 redis-cli --pipe
   ```

   Redis will save this data to disk. On the host we can investigate the mapped direct which should contain the Redis data file.

   ```sh
   ls /docker/redis-data
   ```

   This same directory can be mounted to a second container. One usage is to have a Docker Container performing backup operations on your data.

   ```
   docker run -v /docker/redis-data:/backup ubuntu ls /backup
   ```

2. **Shared Volumes**

   Data Volumes mapped to the host are great for persisting data. However, to gain access to them from another container you need to know the exact path which can make it error-prone.

   An alternate approach is to use -volumes-from. The parameter maps the mapped volumes from the source container to the container being launched.
   In this case, we're mapping our Redis container's volume to an Ubuntu container. The /data directory only exists within our Redis container, however, because of -volumes-from our Ubuntu container can access the data.

   ```sh
   docker run --volumes-from r1 -it ubuntu ls /data
   ```

   This allows us to access volumes from other containers without having to be concerned how they're persisted on the host.

3. **Read-only Volumes**

   Mounting Volumes gives the container full read and write access to the directory. You can specify read-only permissions on the directory by adding the permissions :ro to the mount.

   ```sh
   docker run -v /docker/redis-data:/data:ro -it ubuntu rm -rf /data
   ```

## Managing Log Files

- We can access the standard out and standard error outputs using

```sh
docker logs redis-server
```

- Managing log files
  Docker logs are outputting using the json-file logger meaning the output stored in a JSON file on the host. This can result in large files filling the disk. As a result, you can change the log driver to move to a different destination.

  ```sh
  // command below will redirect the redis logs to syslog
  docker run -d --name redis-syslog --log-driver=syslog redis
  ```

- Disable Logging
  ```sh
  docker run -d --name redis-none --log-driver=none redis
  ```
- Inspect to know which config ?
  ```sh
  docker inspect --format '{{ .HostConfig.LogConfig }}' redis-server
  ```

## Ensuring Uptime (_assurer la disponibilité_)

We can launched some containers, but like any process, containers can crash. we will explore how you can keep containers live and automatically restart them if the crash unexpectedly.

1. **Stop On Fail**
   Docker considers any containers to exit with a non-zero exit code to have crashed. By default a crashed container will remain stopped.
   We can see all container with `docker ps -a`.

2. **Restart On Fail**
   restarting a failed process might correct the problem. Docker can automatically retry to launch the Docker a specific number of times before it stops trying.
   The option `--restart=on-failure:#` allows you to say how many times Docker should try again.
   ```sh
   docker run -d --name restart-3 --restart=on-failure:3 scrapbook/docker-restart-example
   ```
3. **Always Restart**
   Finally Docker can always restart a failed container, in this case, Docker will keep trying until the container it is explicitly told to stop.
   ```sh
   docker run -d --name restart-always --restart=always scrapbook/docker-restart-example
   ```

## Docker Metadata & Labels

When running containers in production, it can be useful to add additional metadata relating to the container to help their management. This metadata could be related to which version of the code is running, which applications or users own the container or define special criteria such as which servers they should run on.

This additional data is managed via Docker Labels. Labels allow you to define custom metadata about a container or image which can later be inspected or used as part of a filter.

1. **Container**

- **Single Label**
  To add a single label you use the `l =<value>` option. The example below assigns a label called user with an ID to the container. This would allow us to query for all the containers running related to that particular user.

      ```sh
      docker run -l user=12345 -d redis
      ```

  - **External File**
    If you're adding multiple labels, then these can come from an external file. The file needs to have a label on each line, and then these will be attached to the running container
    `echo 'user=123461' >> labels && echo 'role=cache' >> labels`

    ```sh
    docker run --label-file=labels -d redis
    ```

2.  **Docker Images**

    Work in the same way as containers but are set in the Dockerfile when the image is built.

    - **Single Label**

          ```sh
          LABEL vendor=Katacoda
          ```

    - **Multiple Labels**

      ```sh
      LABEL vendor=Katacoda \ com.katacoda.version=0.0.5 \ com.katacoda.build-date=2016-07-01T10:47:29Z \ com.katacoda.course=Docker
      ```

3.  **Inspect**

    The first approach to viewing all the labels for a particular container or image is by using `docker inspect`.

    - Container :
      - query all metadata `docker inspect rd`
      - filtering metaData `docker inspect -f "{{json .config.labels}}" rd`
    - Image :
      same way
      - `docker inspect -f "{{json .ContainerConfig.Labels }}" katacoda-label-example`

4.  **Query By Label**

    - **Filtering Containers** : `docker ps --filter "label=user=scrapbook"`
    - **Filtering Images** : `docker images --filter "label=vendor=katacoda"`
      > Note
      > When querying both the label key name and value are case sensitive. This is why it's important to follow a consistent pattern and the recommended guidelines.

5.  **Daemon labels**
    ```sh
    docker -d \
    -H unix:///var/run/docker.sock \
    --label com.katacoda.environment="production" \
    --label com.katacoda.storage="ssd"
    ```

## Load Balancing Containers

1. **NGINX Proxy**

   We want to have a NGINX service running which can dynamically discovery and update its load balance configuration when new containers are loaded. Thankfully this has already been created and is called nginx-proxy.

   Nginx-proxy accepts HTTP requests and proxies the request to the appropriate container based on the request Hostname. This is transparent to the user with happens without any additional performance overhead.

   **Properties**

   There are three keys properties required to be configured when launching the proxy container.

   The first is binding the container to port 80 on the host using -p 80:80. This ensures all HTTP requests are handled by the proxy.

   The second is to mount the `docker.sock` file. This is a connection to the Docker daemon running on the host and allows containers to access its metadata via the API. Nginx-proxy uses this to listen for events and then updates the NGINX configuration based on the container IP address. Mounting file works in the same way as directories using `-v /var/run/docker.sock:/tmp/docker.sock:ro`. We specify :ro to restrict access to read-only.

   Finally, we can set an optional `-e DEFAULTHOST=<domain>`. If a request comes in and doesn't make any specified hosts, then this is the container where the request will be handled. This enables you to run multiple websites with different domains on a single machine with a fall-back to a known website.

   ```sh
   docker run -d -p 80:80 -e DEFAULT_HOST=proxy.example -v /var/run/docker.sock:/tmp/docker.sock:ro --name nginx jwilder/nginx-proxy
   ```

   > Because we're using a DEFAULT_HOST, any requests which come in will be directed to the container that has been assigned the HOST proxy.example.

2. **Single Host**

   Starting Container
   For Nginx-proxy to start sending requests to a container you need to specify the VIRTUAL_HOST environment variable. This variable defines the domain where requests will come from and should be handled by the container.

   ```sh
   docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server
   ```

3. **Cluster**

   If we launch a second container with the same VIRTUAL_HOST then nginx-proxy will configure the system in a round-robin load balanced scenario. This means that the first request will go to one container, the second request to a second container and then repeat in a circle.
   There is no limit to the number of nodes you can have running.

   ```sh
   docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server
   ```

4. **Generated NGINX Configuration**

   While nginx-proxy automatically creates and configures NGINX for us, if you're interested in what the final configuration looks like then you can output the complete config file with docker exec as shown below.

   ```sh
   docker exec nginx cat /etc/nginx/conf.d/default.conf
   ```

   Additional information `docker logs nginx`

## Orchestration using Docker Compose

When working with multiple containers, it can be difficult to manage the starting along with the configuration of variables and links. To solve this problem, Docker has a tool called Docker Compose to manage the orchestration, of launching, of containers.

1. **Defining First Container**

   Docker Compose is based on a docker-compose.yml file.
   The format of the file is based on YAML (Yet Another Markup Language).

   ```yaml
   container_name:
     property: value
       - or options
   ```

   - Create Node.js

     ```yaml
       web: // define a container called web
         build: . // which is based on the build of the current directory
         links: // To link two containers together to specify a links property and list required connections
           - redis
         ports: // The same format is used for other properties such as ports
           - "3000"
       redis: // second container
         image: redis:alpine
         volumes:
           - /var/redis/data:/data
     ```

     We can launch all the applications with `docker-compose up -d` or a single container with `up <name>`

     > The `-d` argument states to run the containers in the background, similar to when used with docker run.

     **Docker Management**

     - See the details of the launched containers you can use `docker-compose ps`
     - To access all the logs via a single stream you use `docker-compose logs`
     - Other commands follow the same pattern. Discover them by typing `docker-compose`.

     **Docker Scale** :
     The scale option allows you to specify the service and then the number of instances you want. If the number is greater than the instances already running then, it will launch additional containers. If the number is less, then it will stop the unrequired containers.  
     `docker-compose scale web=3`

     **Docker Stop** :
     As when we launched the application, to stop a set of containers you can use the command `docker-compose stop`.  
     To remove all the containers use the command `docker-compose rm`.

## Docker Stats

```sh
docker stats nginx
```

This launches a terminal window which refreshes itself with live data from the container.

To view the stats for all these containers you can use pipes and xargs. A pipe passes the output from one command into the input of another while xargs allows you to provide this input as arguments to a command.

    By combining the two we can take the list of all our running containers provided by docker ps and use them as the argument for docker stats. This gives us an overview of the entire machine's containers.

```sh
docker ps -q | xargs docker stats
```

## Creating optimised Docker Images using Multi-Stage Builds

how to use the multi-stage build functionality to make smaller, more optimised images

1.  Create Dockerfile
    The Multi-Stage feature allows a single Dockerfile to contain multiple stages in order to produce the desired, optimised, Docker Image.
    **Sample Code**
    Start by deploying a sample Golang HTTP Server. This currently using a two staged Docker Build approach. This scenario will create a new Dockerfile that allows the image to be built using a single command.

    `git clone https://github.com/katacoda/golang-http-server.git`

        **Multi-Stage Dockerfile**
        Using the editor, create a Multi-Stage Dockerfile. The first stage using the Golang SDK to build a binary. The second stage copies the resulting binary into a optimised Docker Image.

        ```sh
        // Dockerfile.multi
        # First Stage
        FROM golang:1.6-alpine

        RUN mkdir /app
        ADD . /app/
        WORKDIR /app
        RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

        # Second Stage

        FROM alpine
        EXPOSE 80
        CMD ["/app"]

        # Copy from first stage

        COPY --from=0 /app/main /app
        ```

2.  Build Multi-STage Docker Image
    ```sh
    docker build -f Dockerfile.multi -t golang-app .
    docker images
    docker run -d -p 80:80 golang-app
    curl localhost
    ```

## Formatting PS Output

The format of docker ps can be formatted to only display the information relevant to you.

```sh
docker ps --format '{{.Names}} container is using {{.Image}} image'
// output
<name> contianer us using <image-name> image.

// OR

docker ps --format 'table {{.Names}}\t{{.Image}}'
//output
NAMES               IMAGE
<name>              <image-name>

// OR can be pipe
docker ps -q | xargs docker inspect --format '{{ .Id }} - {{ .Name }} - {{ .NetworkSettings.IPAddress }}'

```

## Rootless Docker

With Docker all the containers are managed via the Docker Daemon. The Daemon controls all aspects of the container lifecycle.  
Previous versions of Docker required that the Daemon started by user with root privileges. This required giving users full access to a machine in order to control and configure Docker. As a result, this exposed potential security risks.

Rootless Docker is a project from Docker that removes the requirement for the Docker Daemon to be started by a root. This creates a more secure environment.

```sh
lowprivuser@host01:/root$ docker ps
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.39/containers/json: dial unix /var/run/docker.sock: connect: permission denied
```

1. **Install Rootless docker** : `curl -sSL https://get.docker.com/rootless | sh`
2. **Started the daemon** :

   ```sh
   // need to add the following environment variables
   export XDG_RUNTIME_DIR=/tmp/docker-1001
   export PATH=/home/lowprivuser/bin:$PATH
   export DOCKER_HOST=unix:///tmp/docker-1001/docker.sock
   
   // RUN
   /home/lowprivuser/bin/dockerd-rootless.sh --experimental --storage-driver vfs
   ```
